package com.rohit21122012.moneytech.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rohit21122012.moneytech.Database.MoneyTechDbHelper;
import com.rohit21122012.moneytech.Fragments.ExpenseFragment;
import com.rohit21122012.moneytech.Fragments.IncomeFragment;
import com.rohit21122012.moneytech.Fragments.ProfileFragment;
import com.rohit21122012.moneytech.R;

public class SettingsActivity extends ActionBarActivity {

    CardView curr, tp, reset, ws;
    TextView currVal, tpVal, wsVal;

//    private void showCurrDialog(){
//        final String currencies[] = {"Rs", "$", "Euro"};
//       // SharedPreferences sp = getApplicationContext().getSharedPreferences("App_settings", MODE_PRIVATE);
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        final SharedPreferences.Editor editor = sp.edit();
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Choose Currency")
//                .setItems(currencies,new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        editor.putString("CURR", currencies[which]);
//                        editor.commit();
//                        currVal.setText(currencies[which]);
//                        IncomeFragment.notifyChangeInRV();
//                        ExpenseFragment.notifyChangeInRV();
////                        dialog.cancel();
//                        Log.i("Settings", currencies[which]);
//                    }
//                })
//                .setCancelable(true);
//        Dialog currDialog = builder.create();
//        currDialog.show();
//    }

    private void showTpDialog(){
        final String timePeriods[] = {"Day", "Week", "Month"};
    //    SharedPreferences sp = getApplicationContext().getSharedPreferences("App_settings", MODE_PRIVATE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        final SharedPreferences.Editor editor = sp.edit();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Time Period Type")
                .setItems(timePeriods,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor.putString("TP", timePeriods[which]);
                        editor.commit();
                        tpVal.setText(timePeriods[which]);
                        IncomeFragment.notifyChangeInRV();
                        ExpenseFragment.notifyChangeInRV();
//                        dialog.cancel();
                    }
                })
                .setCancelable(true);
        Dialog currDialog = builder.create();
        currDialog.show();
    }

    private void showResetDialog(){
        //final SharedPreferences sp = getApplicationContext().getSharedPreferences("App_settings", MODE_PRIVATE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        final SharedPreferences.Editor editor = sp.edit();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Reset!!")
            .setMessage("Are you sure you want to reset?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    editor.clear();
                    editor.commit();
                    tpVal.setText("Day");

                    wsVal.setText("10000");
                    MainActivity.resetDb();
//                    dialog.cancel();
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        Dialog resetDialog = builder.create();
        resetDialog.show();
    }

    private void showWheelSizeDialog(){
        //final SharedPreferences sp = getApplicationContext().getSharedPreferences("App_settings", MODE_PRIVATE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        final SharedPreferences.Editor editor = sp.edit();

        final EditText et = new EditText(this);
        et.setInputType(InputType.TYPE_CLASS_NUMBER);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Set Wheel Size")
                .setMessage("Enter the maximum daily amount")
                .setView(et)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (et.getText().toString().isEmpty() || et.getText().toString().equals("0"))
                            Toast.makeText(SettingsActivity.this, "Enter something non zero", Toast.LENGTH_SHORT).show();
                        else {
                            editor.putString("WS", et.getText().toString());
                            editor.commit();
                            ProfileFragment pf = (ProfileFragment) ProfileFragment.getExistingInstance();
                            if (pf != null) {
                                pf.setWheelSize(et.getText().toString());
                            }
                            wsVal.setText(et.getText().toString());
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }

                );
        Dialog resetDialog = builder.create();
        resetDialog.show();
    }


    private void setOnClicks(){

        tp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTpDialog();
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResetDialog();
            }
        });
        ws.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWheelSizeDialog();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
//        curr = (CardView) findViewById(R.id.currency);
        tp = (CardView) findViewById(R.id.timeperiod);
        ws = (CardView) findViewById(R.id.wheelsize);
        reset = (CardView) findViewById(R.id.reset);

        tpVal = (TextView) findViewById(R.id.timeperiodValue);
        wsVal = (TextView) findViewById(R.id.wheelsizeValue);
      //  SharedPreferences sp = getSharedPreferences("App_settings", MODE_PRIVATE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        TextView t2 = (TextView) findViewById(R.id.developer);
        t2.setMovementMethod(LinkMovementMethod.getInstance());



        String t = sp.getString("TP", null);
        String w = sp.getString("WS", null);

        if(t != null)
            tpVal.setText(t);
        if(w != null)
            wsVal.setText(w);
        setOnClicks();

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_settings, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
