package com.rohit21122012.moneytech.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.rohit21122012.moneytech.Database.MoneyTechContract.*;

/**
 * Created by rohit on 1/14/16.
 */
public class MoneyTechDbHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "MoneyTechContract.db";
    private static final String SQL_CREATE_ITEM = "CREATE TABLE " + Item.TABLE_NAME + " (" +
           Item._ID + " INTEGER PRIMARY KEY, " +
           Item.COLUMN_NAME_TYPE + " TEXT, " +
           Item.COLUMN_NAME_PERIODIC + " INTEGER, " +   //INCOME OR EXPENSE
           Item.COLUMN_NAME_NAME + " TEXT, " +
           Item.COLUMN_NAME_AMOUNT + " REAL, " +
           Item.COLUMN_NAME_RATE + " INTEGER, " +
           Item.COLUMN_NAME_START + " TEXT, " +
           Item.COLUMN_NAME_END + " TEXT )" ;

    private static final String SQL_CREATE_CHANGELOG = "CREATE TABLE " + ChangeLog.TABLE_NAME + " (" +
           ChangeLog._ID + "INTEGER PRIMARY KEY, " +
           ChangeLog.COLUMN_NAME_TIME + " TEXT, " +
           ChangeLog.COLUMN_NAME_ACTION + " INT, " +
           ChangeLog.COLUMN_NAME_ITEM_ID + " INT )";

    private static final String SQL_DELETE_ITEM = "DROP TABLE IF EXISTS " + Item.TABLE_NAME;
    private static final String SQL_DELETE_CHANGELOG = "DROP TABLE IF EXISTS " + ChangeLog.TABLE_NAME;

    public MoneyTechDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ITEM);
        db.execSQL(SQL_CREATE_CHANGELOG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ITEM);
        db.execSQL(SQL_DELETE_CHANGELOG);
        onCreate(db);
    }

}
