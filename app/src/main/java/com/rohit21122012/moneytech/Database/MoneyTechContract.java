package com.rohit21122012.moneytech.Database;

import android.provider.BaseColumns;

/**
 * Created by rohit on 1/14/16.
 */
public final class MoneyTechContract {

    public MoneyTechContract(){}

    public static abstract class Item implements BaseColumns {
        public static final String TABLE_NAME = "item";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_NAME  = "name";
        public static final String COLUMN_NAME_PERIODIC = "periodic";
        public static final String COLUMN_NAME_RATE = "rate";
        public static final String COLUMN_NAME_AMOUNT = "amount";
        public static final String COLUMN_NAME_START = "start";
        public static final String COLUMN_NAME_END = "end";
    }

    public static abstract class ChangeLog implements BaseColumns{
        public static final String TABLE_NAME = "changelog";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_ACTION = "action";
        public static final String COLUMN_NAME_ITEM_ID = "itemid";
    }
}
