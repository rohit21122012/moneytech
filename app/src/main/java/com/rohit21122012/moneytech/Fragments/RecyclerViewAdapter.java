package com.rohit21122012.moneytech.Fragments;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.wheelindicatorview.WheelIndicatorView;
import com.rohit21122012.moneytech.Activities.MainActivity;
import com.rohit21122012.moneytech.Database.MoneyTechContract;
import com.rohit21122012.moneytech.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by rohit on 12/29/15.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public class Item{
        String name;
        String amount;
        int rate;
        long id;
        Item(String n, String a, int r, long i){
            id = i;
            name = n;
            amount = a;
            rate = r;
        }
    }

    private List<Item> mList;
    private FragmentManager mFm;
    private SharedPreferences mSp;
    private String curr_tp;
    private Context mC;

    private class RecycleAsyncTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            String[] projection = {
                    MoneyTechContract.Item._ID,
                    MoneyTechContract.Item.COLUMN_NAME_NAME,
                    MoneyTechContract.Item.COLUMN_NAME_AMOUNT,
                    MoneyTechContract.Item.COLUMN_NAME_RATE,
                    MoneyTechContract.Item.COLUMN_NAME_START};
            SQLiteDatabase sqDb = MainActivity.getDB();

            if(sqDb != null) {
                String[] args = {String.valueOf(misIncome)};

                Cursor c =sqDb.query(MoneyTechContract.Item.TABLE_NAME,
                        projection, " periodic = ?", args, null, null, MoneyTechContract.Item.COLUMN_NAME_START);
                c.moveToFirst();

                while (!c.isAfterLast()) {
                    mList.add(new Item(c.getString(c.getColumnIndexOrThrow(MoneyTechContract.Item.COLUMN_NAME_NAME)),
                            c.getString(c.getColumnIndexOrThrow(MoneyTechContract.Item.COLUMN_NAME_AMOUNT)),
                            c.getInt(c.getColumnIndexOrThrow(MoneyTechContract.Item.COLUMN_NAME_RATE)),
                            c.getLong(c.getColumnIndexOrThrow(MoneyTechContract.Item._ID))));
                    c.moveToNext();
                }
                c.close();
            }else{
                Log.i("RecycleAdapter", "NO WRITABLE DB");
            }
//            notifyDataSetChanged();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            notifyDataSetChanged();
        }
    }



    int misIncome;
    public RecyclerViewAdapter(int isIncome, FragmentManager fm, Context con){
        mC = con;
        misIncome = isIncome;
        mFm = fm;

        mList = new ArrayList<Item>();
        new RecycleAsyncTask().execute();

      //  SharedPreferences mSp = MainActivity.context.getSharedPreferences("App_settings", Context.MODE_PRIVATE);
        SharedPreferences mSp = PreferenceManager.getDefaultSharedPreferences(MainActivity.context);


        String tp = mSp.getString("TP", null);


        if(tp != null){
            curr_tp = "/ "+ tp;
        }else{
            curr_tp = "/ Day";
        }
        Log.i("RecycleAdapter", "ADAPTER FINISHED CONSTRUCTING");
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent,false);
        final ViewHolder vh = new ViewHolder(v);

        ImageView del = (ImageView) v.findViewById(R.id.delete_item);

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove(vh.getPosition());
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        mC.moveToPosition(position);
//        Log.i("onBindLog", String.valueOf(mC.getColumnIndexOrThrow(MoneyTechContract.Item.COLUMN_NAME_NAME)));
//        holder.mTvName.setText(mC.getString(mC.getColumnIndexOrThrow(MoneyTechContract.Item.COLUMN_NAME_NAME)));
//        holder.mTvAmount.setText(mC.getString(mC.getColumnIndexOrThrow(MoneyTechContract.Item.COLUMN_NAME_AMOUNT)));
//        holder.mTvRate.setText(mC.getString(mC.getColumnIndexOrThrow(MoneyTechContract.Item.COLUMN_NAME_RATE)));
//        holder.Id = mC.getInt(mC.getColumnIndexOrThrow(MoneyTechContract.Item._ID));

      //  SharedPreferences mSp = MainActivity.context.getSharedPreferences("App_settings", Context.MODE_PRIVATE);
        SharedPreferences mSp = PreferenceManager.getDefaultSharedPreferences(MainActivity.context);

        String c = mSp.getString("TP",null);
        int multiplier = 1;
        if(c == "Week"){
            Log.d("mul","7");
            multiplier = 7;
        }else if(c == "Month"){
            Log.d("mul","30");
            multiplier = 30;
        }
//        Log.d("mulrva",String.valueOf(multiplier));
        Double am = Double.parseDouble(mList.get(position).amount);
        am = (double)Math.round(am*100)/100;
        am = am * multiplier;
        NumberFormat format = NumberFormat.getCurrencyInstance();

        holder.id = mList.get(position).id;
        holder.mTvName.setText(mList.get(position).name);
        holder.mTvAmount.setText(format.format(am));
        holder.mTvCurrTp.setText(curr_tp);
//        holder.mTvRate.setText(Integer.toString()mList.get(position).rate);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void add(String name, String amount, int periodicity){
        //Add to DB
        Double am = 0.0;
        switch (periodicity) {
            case R.id.rb_daily :
                am = Double.parseDouble(amount);
                break;
            case R.id.rb_annually :
               am = Double.parseDouble(amount)/(365);
                break;
            case R.id.rb_monthly :
                am = Double.parseDouble(amount)/(30);
                break;
        }

        SQLiteDatabase sqDb = MainActivity.getDB();
        if(sqDb != null) {

            ContentValues values = new ContentValues();
            values.put(MoneyTechContract.Item.COLUMN_NAME_NAME, name);
            values.put(MoneyTechContract.Item.COLUMN_NAME_AMOUNT, String.valueOf(am));
//            values.put(MoneyTechContract.Item.COLUMN_NAME_RATE, periodicity);
            values.put(MoneyTechContract.Item.COLUMN_NAME_PERIODIC, misIncome);
            values.put(MoneyTechContract.Item.COLUMN_NAME_START, Calendar.getInstance().getTime().toString());


            long rowId = sqDb.insertOrThrow(MoneyTechContract.Item.TABLE_NAME, null, values);

            //Add to list
            am = (double)Math.round(am*100)/100;
            mList.add(new Item(name,String.valueOf(am), periodicity, rowId));
           // Log.i("adapterlog", "Added amount " + String.valueOf(amount));

            //Notify RecycleAdapter
            notifyItemInserted(getItemCount());
            ProfileFragment pf = (ProfileFragment)ProfileFragment.getExistingInstance();
                if(pf!= null){
                pf.updateWheels();
            }
        }
    }



    public void remove(int itemPos){
        SQLiteDatabase sqDb = MainActivity.getDB();
        if(sqDb != null) {

            long id = mList.get(itemPos).id;
            mList.remove(itemPos);
            notifyItemRemoved(itemPos);
            String[] args = {String.valueOf(id)};
            sqDb.delete(MoneyTechContract.Item.TABLE_NAME,
                    MoneyTechContract.Item._ID + " LIKE ? ", args);
            ProfileFragment pf = (ProfileFragment)ProfileFragment.getExistingInstance();
            if(pf!= null){
                pf.updateWheels();
            }
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView mCardView;
        TextView mTvName;
        TextView mTvAmount;
        TextView mTvRate;
        TextView mTvCurrTp;
        long id;

        public ViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView)itemView.findViewById(R.id.cardView);
            mTvName = (TextView)itemView.findViewById(R.id.cardView_name);
            mTvAmount = (TextView) itemView.findViewById(R.id.cardView_amount);
            mTvRate = (TextView) itemView.findViewById(R.id.cardView_rate);
            mTvCurrTp = (TextView) itemView.findViewById(R.id.cardView_curr_tp);
        }


    }
}
