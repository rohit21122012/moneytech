package com.rohit21122012.moneytech.Fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.rohit21122012.moneytech.R;

public class EditNameFragment extends DialogFragment {
    static EditNameFragment newInstance() {
        return new EditNameFragment();
    }

    EditText et;
    Button okButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().setTitle("Change user name");
        final ProfileFragment ifr = (ProfileFragment)getTargetFragment();
        View v = inflater.inflate(R.layout.fragment_edit_name, container, false);

        et = (EditText)v.findViewById(R.id.et_prof_name);
        okButton = (Button)v.findViewById(R.id.b_change);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et.getText().toString().isEmpty())
                    Toast.makeText(getActivity().getApplicationContext(), "Enter something", Toast.LENGTH_SHORT).show();
                else {
                    ifr.setProfNameToSharedPref(et.getText().toString());
                    EditNameFragment.this.dismiss();
                }
            }
        });
        return v;
    }
}