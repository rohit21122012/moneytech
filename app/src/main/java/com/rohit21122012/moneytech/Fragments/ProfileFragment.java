package com.rohit21122012.moneytech.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dlazaro66.wheelindicatorview.WheelIndicatorItem;
import com.dlazaro66.wheelindicatorview.WheelIndicatorView;
import com.rohit21122012.moneytech.Activities.MainActivity;
import com.rohit21122012.moneytech.Database.MoneyTechContract;
import com.rohit21122012.moneytech.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.NumberFormat;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SECTION_NUMBER = "section_number";

    private String mSectionNumber;

    private static ProfileFragment fragment = null;

    public static Fragment getExistingInstance(){
        if(fragment == null){
             return null;
        }
        return fragment;
    }


    private OnFragmentInteractionListener mListener;
    Double dailyExpAmount = 0d, dailyIncAmount = 0d;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sectionNumber Parameter 1.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment newInstance(String sectionNumber) {
        fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mSectionNumber = getArguments().getString(ARG_SECTION_NUMBER);
        }


    }

    void showDialog() {
        // Create the fragment and show it as a dialog.
        EditNameFragment newFragment = EditNameFragment.newInstance();
        newFragment.setTargetFragment(this,1);
        newFragment.show(getFragmentManager(), "etdialog");
    }

    void updateWheels(){
        new WheelLoader().execute();
    }

    public void setWheelSize(String val){
        Double v = Double.parseDouble(val);
        int num = v.intValue();
        if(num != 0) {
            wivA.setFilledPercent(dailyExpAmount.intValue() *100 / num);
            wivB.setFilledPercent(dailyIncAmount.intValue() *100 / num);
            wivA.startItemsAnimation();
            wivB.startItemsAnimation();
        }
    }

    CircleImageView profilePic;
    TextView profileName;

    WheelIndicatorView wivA, wivB;
    TextView expVal, incVal, net;
    ImageView editpp, editname;

    TextView per1, per2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.test_fragment_profile, container, false);
        profilePic = (CircleImageView) v.findViewById(R.id.profile_image);
        getPhotoFromSharedPref();

        profileName = (TextView) v.findViewById(R.id.profile_name);
        getProfNameFromSharedPref();

        incVal = (TextView) v.findViewById(R.id.income_val);
        expVal = (TextView) v.findViewById(R.id.expense_val);
        net = (TextView) v.findViewById(R.id.net_rate);
        per1 = (TextView) v.findViewById(R.id.per_tp1);
        per2 = (TextView) v.findViewById(R.id.per_tp2);


        wivA = (WheelIndicatorView) v.findViewById(R.id.wheel_1);

        wivB = (WheelIndicatorView) v.findViewById(R.id.wheel_2);

        updateWheels();

        editname = (ImageView)v.findViewById(R.id.edit_name);
        editpp = (ImageView) v.findViewById(R.id.edit_camera);
        editpp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAlert(2);
            }
        });

        editname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

//        profileName.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                showDialog();
//                return true;
//            }
//        });
//
//        profilePic.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                createAlert(2);
//                return true;
//            }
//        });
//        ValueLineChart mCubicValueLineChart = (ValueLineChart) v.findViewById(R.id.cubiclinechart);
//
//        ValueLineSeries series = new ValueLineSeries();
//        series.setColor(0xFF56B7F1);
//
//        series.addPoint(new ValueLinePoint("Jan", 2.4f));
//        series.addPoint(new ValueLinePoint("Feb", 3.4f));
//        series.addPoint(new ValueLinePoint("Mar", .4f));
//        series.addPoint(new ValueLinePoint("Apr", 1.2f));
//        series.addPoint(new ValueLinePoint("Mai", 2.6f));
//        series.addPoint(new ValueLinePoint("Jun", 1.0f));
//        series.addPoint(new ValueLinePoint("Jul", 3.5f));
//        series.addPoint(new ValueLinePoint("Aug", 2.4f));
//        series.addPoint(new ValueLinePoint("Sep", 2.4f));
//        series.addPoint(new ValueLinePoint("Oct", 3.4f));
//        series.addPoint(new ValueLinePoint("Nov", .4f));
//        series.addPoint(new ValueLinePoint("Dec", 1.3f));
//
//        mCubicValueLineChart.addSeries(series);
//        mCubicValueLineChart.startAnimation();

        return v;
    }

    private class WheelLoader extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... params) {
            SQLiteDatabase sqDb = MainActivity.getDB();
            if(sqDb != null) {
                Cursor c = sqDb.rawQuery("SELECT SUM(amount) AS total_daily  FROM " + MoneyTechContract.Item.TABLE_NAME + " WHERE periodic = ? ",  new String[] {"1"});
                c.moveToFirst();
                dailyExpAmount = c.getDouble(c.getColumnIndex("total_daily"));
                Cursor c2 = sqDb.rawQuery("SELECT SUM(amount) AS total_daily  FROM " + MoneyTechContract.Item.TABLE_NAME + " WHERE periodic = ? ",  new String[] {"2"});
                c2.moveToFirst();
                dailyIncAmount = c2.getDouble(c2.getColumnIndex("total_daily"));

            }else{
                Log.i("ProfileFragment", "NO WRITABLE DB");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            SharedPreferences mSp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

//            SharedPreferences mSp = MainActivity.context.getSharedPreferences("App_settings", Context.MODE_PRIVATE);


            String c = mSp.getString("TP",null);
            String w = mSp.getString("WS",null);
            int multiplier = 1;
            if(c == null) {
                per1.setText("per Day");
                per2.setText("per Day");
            }else if(c.equals("Week")){
                multiplier = 7;
                per1.setText("per " + c);
                per2.setText("per " + c);
            }else if(c.equals("Month")){
                multiplier = 30;
                per1.setText("per " + c);
                per2.setText("per " + c);
            }

            dailyExpAmount *= multiplier;
            dailyIncAmount *= multiplier;



            int wheelSize;
            if(w == null){
                wheelSize = 10000 ;
            }else{
                Double v = Double.parseDouble(w);

                wheelSize = v.intValue() ;
            }
            if(wheelSize == 0) wheelSize = 1;

           // Log.i("pf", dailyExpAmount.toString() + " " + String.valueOf(dailyExpAmount.intValue()));
            dailyExpAmount = (double)Math.round(dailyExpAmount * 100) / 100;
            NumberFormat formatter = NumberFormat.getCurrencyInstance();

            expVal.setText(formatter.format(dailyExpAmount));

            dailyIncAmount = (double)Math.round(dailyIncAmount * 100) / 100;
            incVal.setText(formatter.format(dailyIncAmount));


            wivA.setFilledPercent(dailyExpAmount.intValue() *100/ wheelSize);
            WheelIndicatorItem wivAItem1 = new WheelIndicatorItem(1.0f, Color.RED);
            wivA.addWheelIndicatorItem(wivAItem1);

            wivA.startItemsAnimation();
            wivB.setFilledPercent(dailyIncAmount.intValue() *100/ wheelSize);
            WheelIndicatorItem wivAItem2 = new WheelIndicatorItem(1.0f, Color.GREEN);
            wivB.addWheelIndicatorItem(wivAItem2);
            wivB.startItemsAnimation();
            Double netVal = dailyIncAmount - dailyExpAmount;
            netVal = (double) Math.round(netVal * 100) / 100;
            net.setText(formatter.format(netVal));
        }
    }

    private void createAlert(final int type){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage("Want to change?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                    new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    dispatchTakePictureIntent();
                    dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void setProfNameToSharedPref(String name){
        //SharedPreferences shared = getActivity().getSharedPreferences("App_settings", Context.MODE_PRIVATE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        SharedPreferences.Editor editor = sp.edit();
        editor.putString("PROF_NAME", name);
        editor.commit();
        profileName.setText(name);
    }

    private void getProfNameFromSharedPref(){
        //SharedPreferences sp = getActivity().getSharedPreferences("App_settings", Context.MODE_PRIVATE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        String name = sp.getString("PROF_NAME", null);
        if(name != null){
            profileName.setText(name);
        }
    }


    private void getPhotoFromSharedPref(){
       // SharedPreferences sp = getActivity().getSharedPreferences("App_settings", Context.MODE_PRIVATE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        String photo = sp.getString("PROF_PIC", null);
        if(photo != null){
            byte[] b = Base64.decode(photo, Base64.DEFAULT);
            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(b));
            profilePic.setImageBitmap(bm);
        }
    }

    private void addPhotoToSharedPref(Bitmap bm){
     //   SharedPreferences shared = getActivity().getSharedPreferences("App_settings", Context.MODE_PRIVATE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        SharedPreferences.Editor editor = sp.edit();
        editor.putString("PROF_PIC", encodeTobase64(bm));
        editor.commit();
    }

    private String encodeTobase64(Bitmap bm){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        return Base64.encodeToString(baos.toByteArray(),Base64.DEFAULT);
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            profilePic.setImageBitmap(imageBitmap);
            addPhotoToSharedPref(imageBitmap);
        }
    }


//    String mCurrentPhotoPath;
//    static final int REQUEST_TAKE_PHOTO = 1;

//    private void dispatchTakePictureIntent() {
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        // Ensure that there's a camera activity to handle the intent
//        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
//            // Create the File where the photo should go
//            File photoFile = null;
//            try {
//                photoFile = createImageFile();
//            } catch (IOException ex) {
//                // Error occurred while creating the File
//
//            }
//            // Continue only if the File was successfully created
//            if (photoFile != null) {
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//                        Uri.fromFile(photoFile));
//
//                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
//            }
//        }
//    }

//    private void setPic() {
//        // Get the dimensions of the View
//        int targetW = profilePic.getWidth();
//        int targetH = profilePic.getHeight();
//
//        // Get the dimensions of the bitmap
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        bmOptions.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
//        int photoW = bmOptions.outWidth;
//        int photoH = bmOptions.outHeight;
//
//        // Determine how much to scale down the image
//        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
//
//        // Decode the image file into a Bitmap sized to fill the View
//        bmOptions.inJustDecodeBounds = false;
//        bmOptions.inSampleSize = scaleFactor;
//        bmOptions.inPurgeable = true;
//
//        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
//        profilePic.setImageBitmap(bitmap);
//    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
//            setPic();
//        }
//    }

//
//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }



//    private File createImageFile() throws IOException {
//        // Create an image file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES);
//        File image = File.createTempFile(
//                imageFileName,  /* prefix */
//                ".jpg",         /* suffix */
//                storageDir      /* directory */
//        );
//
//        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
//        return image;
//    }
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
