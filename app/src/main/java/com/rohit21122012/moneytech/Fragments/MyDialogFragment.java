package com.rohit21122012.moneytech.Fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.rohit21122012.moneytech.R;

public class MyDialogFragment extends DialogFragment {
    static MyDialogFragment newInstance(int isIncome) {
        misIncome = isIncome;
        return new MyDialogFragment();

    }

    EditText nameEt, rateEt, amountEt;
    Button okButton;
    RadioGroup rg;
    static int misIncome;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_dialog, container, false);
        getDialog().setTitle("Add income/expense");
        nameEt = (EditText) v.findViewById(R.id.et_name);
        amountEt = (EditText) v.findViewById(R.id.et_amount);
        rg = (RadioGroup) v.findViewById(R.id.rg);

//        rateEt = (EditText) v.findViewById(R.id.et_rate);


        okButton = (Button) v.findViewById(R.id.ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nameEt.getText().toString().isEmpty() ||
                        amountEt.getText().toString().isEmpty() || rg.getCheckedRadioButtonId() == -1
                       ){
                    Toast.makeText(getActivity().getApplicationContext(), "Enter Something", Toast.LENGTH_SHORT).show();
                }else {
//                    int periodicity = 0;
//                    switch (rg.getCheckedRadioButtonId()){
//                        case R.id.rb_daily:
//                            periodicity = 1;
//                            break;
//                        case R.id.rb_monthly:
//                            periodicity = 2;
//                            break;
//                        case R.id.rb_annually:
//                            periodicity = 3;
//                            break;
//                    }
//                Log.i("MDF Amount", amountEt.getText().toString());
                if(misIncome == 1) {
                    IncomeFragment ifr = (IncomeFragment) getTargetFragment();
                    ifr.addToList(nameEt.getText().toString(), amountEt.getText().toString(), rg.getCheckedRadioButtonId());
                }else if(misIncome == 2){
                    ExpenseFragment efr= (ExpenseFragment) getTargetFragment();
                    efr.addToList(nameEt.getText().toString(), amountEt.getText().toString(), rg.getCheckedRadioButtonId());
                }
                    MyDialogFragment.this.dismiss();
                }
             }
        });
        return v;
    }
}